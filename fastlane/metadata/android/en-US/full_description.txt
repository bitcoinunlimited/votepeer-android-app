VotePeer allows you to rapidly set up, hold a vote or a poll on top of the Bitcoin Cash blockchain that implements the two option vote Bitcoin Cash smart contract.

No registration is required, use the android app and scan a QR code to log in.

Back up your mnemonic!

Uninstalling the app will result in a loss of keys and a generation of new ones.

https://voter.cash
https://docs.voter.cash