package info.bitcoinunlimited.voting.messaging

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import bitcoinunlimited.libbitcoincash.Secret
import com.google.firebase.messaging.RemoteMessage
import info.bitcoinunlimited.voting.VotePeerMock
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import info.bitcoinunlimited.voting.wallet.room.WalletDatabase
import io.mockk.every
import io.mockk.mockk
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class MyMessagingServiceTest {
    private lateinit var mnemonic: Mnemonic
    private lateinit var myMessagingService: MyMessagingService
    private val electionIdMock = "election_id_mock_value"
    private lateinit var privateKeyMock: Secret
    private lateinit var remoteMessageMock: RemoteMessage
    private lateinit var applicationContextMock: Context

    @Before
    fun setUp() = runBlockingTest {
        mnemonic = Mnemonic(
            phrase = "mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic " +
                "mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic mnemonic"
        )
        applicationContextMock = ApplicationProvider.getApplicationContext() as Context
        val applicationContext = ApplicationProvider.getApplicationContext() as Context
        // val mnemonic = MnemonicDatabase.getInstance(applicationContext).getMnemonic()
        val payDestination = WalletDatabase(applicationContext).getVoterAddress()
        myMessagingService = MyMessagingService(applicationContext, payDestination)
        privateKeyMock = VotePeerMock.mockPrivateKey()

        val pushTypeKey = "pushType"
        val pushTypeMock = "on_create_election"
        val electionIdKey = "election_id"
        val electionIdMock = "election_id_mock_value"
        val remoteMessageData = mapOf(Pair(electionIdKey, electionIdMock), Pair(pushTypeKey, pushTypeMock))
        remoteMessageMock = mockk(relaxed = true)
        every { remoteMessageMock.data } returns remoteMessageData
    }

    @Test
    fun `handleRemoteMessage$app_debug`() {
        // TODO: Implement
    }

//    @Test
//    fun getElectionIdNoData() {
//        val remoteMessageMock = mockk<RemoteMessage>(relaxed = true)
//        val exception = assertThrows<Exception> {
//            myMessagingService.getElectionId(remoteMessageMock)
//        }
//        assertEquals(exception.message, "election_id is missing from remoteMessage data: $remoteMessageMock")
//    }

    @Test
    fun getElectionId() {
        val electionId = myMessagingService.getElectionId(remoteMessageMock)
        assertEquals(electionIdMock, electionId)
    }

    @Test
    fun getPushType() = runBlockingTest {
        val pushTypeKey = "pushType"
        val pushTypeMock = "on_create_election"
        val remoteMessageData = mapOf(Pair(pushTypeKey, pushTypeMock))
        val remoteMessageMock = mockk<RemoteMessage>(relaxed = true)
        every { remoteMessageMock.data } returns remoteMessageData

        val pushType = myMessagingService.getPushType(remoteMessageMock)
        assertEquals(pushTypeMock, pushType)
    }

//    @Test
//    fun getPushTypeNoData() {
//        val remoteMessageMock = mockk<RemoteMessage>(relaxed = true)
//        val exception = assertThrows<Exception> {
//            myMessagingService.getPushType(remoteMessageMock)
//        }
//        assertEquals(exception.message, "pushType is missing from remoteMessage.data: $remoteMessageMock")
//    }
//
//    @Test
//    fun `getVotePeerActivityIntent$app_debug`() {
//        val intent = InjectorUtilsApp.getVotePeerActivityIntent(
//            applicationContextMock, privateKeyMock, electionIdMock, mnemonic.phrase
//        )
//        assertEquals(intent.extras?.get(Constants.PRIVATE_KEY), privateKeyMock.getSecret()!!)
//        assertEquals(intent.extras?.get("setSupportActionBar"), false)
//        assertEquals(intent.extras?.get(Constants.SETUP_ACTION_BAR_WITH_NAV_CONTROLLER), false)
//        assertEquals(intent.extras?.get("setContentView"), false)
//        assertEquals(intent.extras?.get("election_id"), electionIdMock)
//        assertNotNull(intent.flags)
//    }
//
//    @Test
//    fun `getVotePeerActivityPendingIntent$app_debug`() {
//        val intent = InjectorUtilsApp.getVotePeerActivityIntent(
//            applicationContextMock, privateKeyMock, electionIdMock, mnemonic.phrase
//        )
//        val pendingIntent = myMessagingService.getVotePeerActivityPendingIntent(intent)
//        assertNotNull(pendingIntent)
//    }
//
//    @Test
//    fun `createNotificationBuilder$app_debug`() {
//        val content = "election_id"
//        val intent = InjectorUtilsApp.getVotePeerActivityIntent(
//            applicationContextMock, privateKeyMock, electionIdMock, mnemonic.phrase
//        )
//        val pendingIntent = myMessagingService.getVotePeerActivityPendingIntent(intent)
//        val notificationBuilder = myMessagingService.createNotificationBuilder(content, pendingIntent)
//        assertNotNull(notificationBuilder)
//    }

    @Test
    fun `createNotification$app_debug`() = runBlocking {
        val content = "election_id"
        val intent = InjectorUtilsApp.getVotePeerActivityIntent(
            applicationContextMock, privateKeyMock, electionIdMock, mnemonic.phrase
        )
        val pendingIntent = myMessagingService.getVotePeerActivityPendingIntent(intent)
        val notificationBuilder = myMessagingService.createNotificationBuilder(content, pendingIntent)
        myMessagingService.createNotification(notificationBuilder)
    }
}
