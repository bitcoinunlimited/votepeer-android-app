package info.bitcoinunlimited.voting

import bitcoinunlimited.libbitcoincash.*

object VotePeerMock {
    fun mockPrivateKey(fill: Char = 'A'): Secret = UnsecuredSecret(ByteArray(32) { _ -> fill.toByte() })

    fun mockP2pkhAddress(chain: ChainSelector, privateKey: Secret): PayAddress {
        val pkh = toPKH(PayDestination.GetPubKey(privateKey.getSecret()!!))
        return PayAddress(chain, PayAddressType.P2PKH, pkh)
    }

    private fun toPKH(publicKey: ByteArray): ByteArray {
        return Hash.hash160(publicKey)
    }
}
