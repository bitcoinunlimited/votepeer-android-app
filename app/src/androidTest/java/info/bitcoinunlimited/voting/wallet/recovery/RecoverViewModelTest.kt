package info.bitcoinunlimited.voting.wallet.recovery

import android.app.Application
import io.mockk.* // ktlint-disable no-wildcard-imports
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
internal class RecoverViewModelTest {
    private lateinit var viewModel: RecoverViewModel
    private lateinit var application: Application
    private val recoveryViewStateMock = RecoverViewState.Recovery

    @Before
    fun setUp() = runBlocking {
        application = mockk(relaxed = true)
        viewModel = spyk(RecoverViewModel(application))
    }

    @After
    fun tearDown() {
        viewModel.state.value = null
    }

    @Test
    fun bindIntents() {
        val recoverFragment = mockk<RecoverFragment>(relaxed = true)
        viewModel.state.value = recoveryViewStateMock
        viewModel.bindIntents(recoverFragment)
        coVerify { viewModel.state.filterNotNull() }
        assertEquals(recoveryViewStateMock, viewModel.state.value)
    }
}
