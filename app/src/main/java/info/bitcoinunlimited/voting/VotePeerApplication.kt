package info.bitcoinunlimited.voting

import android.app.Application
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
open class VotePeerApplication : Application() {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }
}
