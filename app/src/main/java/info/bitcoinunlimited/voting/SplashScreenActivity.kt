package info.bitcoinunlimited.voting

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import bitcoinunlimited.libbitcoincash.Secret
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import info.bitcoinunlimited.voting.utils.TAG_SPLASH_SCREEN_ACTIVITY
import info.bitcoinunlimited.voting.wallet.room.MnemonicDatabase
import info.bitcoinunlimited.voting.wallet.room.WalletDatabase
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)
        initPrivateKey()
    }

    private val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_SPLASH_SCREEN_ACTIVITY, "${SplashScreenActivity::class}: ${exception.message}")
        renderErrorState(exception)
        throw exception
    }

    private fun initPrivateKey() = GlobalScope.launch(Dispatchers.IO + handler) {
        val voterSecret = WalletDatabase(applicationContext).getVoterSecret()
        val mnemonic = MnemonicDatabase.getInstance(applicationContext).getMnemonic()
        startVotingActivity(voterSecret, mnemonic)
    }

    private fun startVotingActivity(privateKey: Secret, mnemonic: String) = GlobalScope.launch(Dispatchers.Main + handler) {
        val intent = InjectorUtilsApp.getVotingActivityIntent(applicationContext, privateKey, intent, mnemonic)
        startActivity(intent)
    }

    private fun renderErrorState(exception: Throwable) {
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.something_went_wrong))
            .setMessage(exception.message)
            .setNeutralButton(resources.getString(R.string.got_it)) { _, _ ->
                // Do nothing.
            }
            .show()
    }
}
