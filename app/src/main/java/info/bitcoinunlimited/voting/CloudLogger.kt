package info.bitcoinunlimited.voting

import com.google.firebase.crashlytics.FirebaseCrashlytics

object CloudLogger {
    fun recordException(exception: Throwable) {
        FirebaseCrashlytics.getInstance().recordException(exception)
    }
}
