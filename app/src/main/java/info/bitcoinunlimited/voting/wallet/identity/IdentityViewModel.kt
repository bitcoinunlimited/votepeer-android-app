package info.bitcoinunlimited.voting.wallet.identity

import android.graphics.Bitmap
import android.graphics.Color
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.PayAddress
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.voting.CloudLogger
import info.bitcoinunlimited.voting.utils.TAG_IDENTITY
import info.bitcoinunlimited.voting.wallet.identity.IdentityViewState.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
open class IdentityViewModel(
    private val electrumAPI: ElectrumAPI,
    internal val address: PayAddress,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {
    private val addressHuman = address.toString()
    internal val state = MutableStateFlow<IdentityViewState?>(null)

    internal val handler = CoroutineExceptionHandler { _, exception ->
        Log.e(TAG_IDENTITY, exception.message ?: "Something went wrong in IdentityViewModel")
        CloudLogger.recordException(exception)
        state.value = Problem(exception)
    }

    init {
        viewModelScope.launch(dispatcher + handler) {
            fetchBalance()
        }
    }

    internal fun setState(newState: IdentityViewState) = viewModelScope.launch(Dispatchers.Main + handler) {
        state.value = newState
    }

    internal fun bindIntents(view: IdentityView) {
        view.initState().onEach {
            state.filterNotNull().collect {
                view.render(it)
            }
        }.launchIn(viewModelScope + handler)
    }

    internal fun initAddress() {
        val qrHeight = 512
        val qrWidth = 512
        val qrCode = generateQrCode(address.toString(), qrHeight, qrWidth)
        setState(Identity(address.toString(), qrCode))
    }

    internal suspend fun subscribeAddress() {
        electrumAPI.subscribeToAddress(addressHuman) {
            viewModelScope.launch(dispatcher + handler) {
                fetchBalance()
            }
        }
    }

    internal suspend fun fetchBalance() {
        setState(BalanceProgressbar(true))
        val result = electrumAPI.getBalance(address)
        val confirmedSatoshis = result.confirmed
        val unConfirmedSatoshis = result.unconfirmed
        val balance = confirmedSatoshis + unConfirmedSatoshis
        setState(Balance(balance))
        setState(BalanceProgressbar(false))
    }

    internal fun generateQrCode(address: String, qrWidth: Int, qrHeight: Int): Bitmap {
        if (address.isEmpty()) throw IllegalArgumentException(addressEmpty)
        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(address, BarcodeFormat.QR_CODE, qrWidth, qrHeight)
        val bitmap = Bitmap.createBitmap(bitMatrix.width, bitMatrix.height, Bitmap.Config.RGB_565)
        for (x in 0 until bitMatrix.width) {
            for (y in 0 until bitMatrix.height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        return bitmap
    }

    internal fun unsubscribeAddress() {
        GlobalScope.launch(dispatcher + handler) {
            electrumAPI.unsubscribeAddress(addressHuman)
        }
    }

    companion object {
        internal const val addressEmpty = "Address is empty, cannot generate QR-code"
    }
}
