package info.bitcoinunlimited.voting.wallet.identity

import android.graphics.Bitmap
import info.bitcoinunlimited.voting.utils.VotingConstants
import java.math.BigDecimal

sealed class IdentityViewState {
    data class BalanceProgressbar(
        val enabled: Boolean = false
    ) : IdentityViewState()

    data class Balance(
        val balanceSatoshi: Int,
        val balance: BigDecimal = (balanceSatoshi / VotingConstants.SATOSHI_RATIO).toBigDecimal(),
        val balanceHuman: String = balance.toPlainString()
    ) : IdentityViewState()

    data class Identity(
        val address: String,
        val qrCode: Bitmap
    ) : IdentityViewState()

    data class Problem(
        val exception: Throwable
    ) : IdentityViewState()
}
