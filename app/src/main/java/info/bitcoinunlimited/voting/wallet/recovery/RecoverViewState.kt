package info.bitcoinunlimited.voting.wallet.recovery

sealed class RecoverViewState {
    object Recovery : RecoverViewState()

    object RecoverySuccess : RecoverViewState()

    data class RecoveryError(
        val message: String
    ) : RecoverViewState()

    data class Recovering(
        val message: String
    ) : RecoverViewState()
}
